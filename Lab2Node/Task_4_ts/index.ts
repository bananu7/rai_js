//Lets require/import the HTTP module
import * as http from 'http';
import * as path from 'path';
import * as fs from 'fs';
import * as url from 'url';
import * as archiver from 'archiver';
//var archiver = require('archiver');

//Lets define a port we want to listen to
const PORT = 8080;

// url:    http://localhost:8080/?folder=images&mask=*.png

/*class ResponseStream implements http.ServerResponse {
    constructor(private response: http.ServerResponse)
    close() : void {
        this.response.end();
    }
}*/
interface ResponseStream extends http.ServerResponse {
    close() : void;
    bytesWritten: number;
    path: string;
}

//We need a function which handles requests and send response
function handleRequest(request: http.ServerRequest, response: http.ServerResponse) {
    var url_parts = url.parse(request.url, true);
    var folder = url_parts.query.folder;    
    var mask = url_parts.query.mask;

    // zip output
    var archive = archiver('zip');   
    archive.on('error', function(err: Error){
        console.error(err);
        process.exit(1);
    });
    
    //(<any>response).close = function() { this.end() };

    archive.pipe(<ResponseStream>response);

    const glob = String(folder) + "/**/" + String(mask)
    console.log(glob);
    archive.glob(glob);
    archive.finalize();
}

//Create a server
var server = http.createServer(handleRequest);

//Lets start our server
server.listen(PORT, function () {
    //Callback triggered when server is successfully listening. Hurray!
    console.log("Server listening on: http://localhost:%s", PORT);
});