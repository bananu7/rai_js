"use strict";
// konstruktor "Pojazd" z atrybutami nazwa, maxpredkosc, predksc i funkcjami start(predkoasc), stop, biezacaPredkosc

// Zad I.1

function Pojazd(nazwa, maxpredkosc) {
  var predkosc = 0;

  this.start = function(_predkosc) {
  if (_predkosc > maxpredkosc) {
    throw new Error("Ten pojazd nie moze jechac tak szybko!");
  }
    predkosc = _predkosc;
  }

  this.stop = function() {
    predkosc = 0;
  }

  this.biezacaPredkosc = function() {
    return predkosc;
  }
}

// Zad I.2

function PojazdProto(nazwa, maxpredkosc) {  
  this.nazwa = nazwa;
  this.maxpredkosc = maxpredkosc; 
}
PojazdProto.prototype.predkosc = 0;
PojazdProto.prototype.start = function start(predkosc) {
  if (predkosc > this.maxpredkosc) {
    throw new Error("Ten pojazd nie moze jechac tak szybko!");
  }
  this.predkosc = predkosc;
};

PojazdProto.prototype.stop = function stop() {
  this.predkosc = 0; 
};

PojazdProto.prototype.biezacaPredkosc = function() {
  return this.predkosc;
};

// Zad I.3

function PojazdCiezarowy(nazwa, maxpredkosc, maxladunek){
  PojazdProto.call(this, nazwa, maxpredkosc);
  this.ladunek = 0;
  this.maxladunek = maxladunek;
}

PojazdCiezarowy.prototype = Object.create(PojazdProto.prototype);

PojazdCiezarowy.prototype.zaladuj = function(ile) {
  if (ile + this.ladunek > this.maxladunek) {
    throw new Error("Proba przekroczenia ladownosci!");
  }
  this.ladunek += ile;
}

PojazdCiezarowy.prototype.rozladuj = function(ile) {
  if (ile > this.ladunek) {
    throw new Error("Rozladowano wiecej niz posiada!");
  }
  this.ladunek -= ile;
}


// Zad I.4

class PojazdC {
  constructor(nazwa, maxpredkosc) {
    this.nazwa = nazwa;
    this.maxpredkosc = maxpredkosc;
    
    this.predkosc = 0;
  }
  
  biezacaPredkosc() {
    return this.predkosc;
  }
  
  start(predkosc) {
    if (predkosc > this.maxpredkosc) {
      throw new Error("Ten pojazd nie moze jechac tak szybko!");
    }
    this.predkosc = predkosc; 
  }
}


class PojazdCiezarowyC extends PojazdC {
  constructor(nazwa, maxpredkosc, maxladunek) {
    super(nazwa, maxpredkosc);
    this.maxladunek = maxladunek;
    this.ladunek = 0;
  }
  
  zaladuj(ile) {
      if (ile + this.ladunek > this.maxladunek) {
        throw new Error("Proba przekroczenia ladownosci!");
    }
    this.ladunek += ile;
  }
  
  rozladuj(ile) {
    if (ile > this.ladunek) {
      throw new Error("Rozladowano wiecej niz posiada!");
    }
    this.ladunek -= ile;
  }
}

module.exports = {
  Pojazd: Pojazd,
  PojazdProto: PojazdProto,
  PojazdCiezarowy: PojazdCiezarowy,
  PojazdC: PojazdC,
  PojazdCiezarowyC: PojazdCiezarowyC
};