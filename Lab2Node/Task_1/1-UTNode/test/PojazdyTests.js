"use strict";
var chai = require("chai");
var expect = chai.expect;

var Pojazdy = require("../src/Pojazdy");
var PojazdCiezarowy = Pojazdy.PojazdCiezarowy;
var PojazdProto = Pojazdy.PojazdProto;
var Pojazd = Pojazdy.Pojazd;
var PojazdC = Pojazdy.PojazdC;
var PojazdCiezarowyC = Pojazdy.PojazdCiezarowyC;

// Zad I.1

describe("Pojazd", function() {
    var p = new Pojazd("p", 10);

    it("properly sets initial speed", function() {
        expect(p.biezacaPredkosc()).equal(0);
    });
    
    it("properly speeds up", function() {
        p.start(5);
        expect(p.biezacaPredkosc()).equal(5);
    });
    
    it("properly verifies maximum speed", function() {
        expect(function() {
            p.start(15)
        }).throw();
    });
});

// Zad I.2

describe("PojazdProto", function() {
    var p = new PojazdProto("p", 10);

    it("properly sets initial speed", function() {
        expect(p.biezacaPredkosc()).equal(0);
    });
    
    it("properly verifies maximum speed", function() {
        expect(function() {
            p.start(15)
        }).throw();
    });
});

// Zad I.3

describe("PojazdCiezarowy", function() {
    var p = new PojazdCiezarowy("pc", 10, 20);

    it("properly sets initial speed", function() {
        expect(p.biezacaPredkosc()).equal(0);
    });
    
    it("properly speeds up", function() {
        p.start(5);
        expect(p.biezacaPredkosc()).equal(5);
    });
    
    it("properly verifies maximum speed", function() {
        expect(function() {
            p.start(15)
        }).throw();
    });
    
    it("properly checks for overloading", function() {
        expect(function() {
            p.zaladuj(25)
        }).throw();
    });
    
    it("properly checks for over-unloading", function() {
        expect(function() {
            p.rozladuj(10)
        }).throw();
    });
});

// Zad I.4

describe("PojazdC", function() {
    var p = new PojazdC("p", 10);

    it("properly sets initial speed", function() {
        expect(p.biezacaPredkosc()).equal(0);
    });
    
    it("properly verifies maximum speed", function() {
        expect(function() {
            p.start(15)
        }).throw();
    });
});

describe("PojazdCiezarowyC", function() {
    var p = new PojazdCiezarowyC("pc", 10, 20);

    it("properly sets initial speed", function() {
        expect(p.biezacaPredkosc()).equal(0);
    });
    
    it("properly speeds up", function() {
        p.start(5);
        expect(p.biezacaPredkosc()).equal(5);
    });
    
    it("properly verifies maximum speed", function() {
        expect(function() {
            p.start(15)
        }).throw();
    });
    
    it("properly checks for overloading", function() {
        expect(function() {
            p.zaladuj(25)
        }).throw();
    });
    
    it("properly checks for over-unloading", function() {
        expect(function() {
            p.rozladuj(10)
        }).throw();
    });
});