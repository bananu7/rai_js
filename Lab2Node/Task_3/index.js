var http = require('http');
var path = require('path');
var fs = require('fs');

const PORT = 8080;

function getPathPrefix(ext) {
    const exts = {
        '.jpg': 'images',
        '.png': 'images',
        '.css': 'styles'
    };
    var p = exts[ext];
    return p ? p : "";
}

function handleRequest(req, res) {
    var extension = path.extname(req.url);

    var pathPrefix = getPathPrefix(extension);

    fs.readFile(pathPrefix + req.url, function(err,data){
        if (err) {
            res.statusCode = 404;
            res.end(String(err));
        } else {
            res.end(data);
        }
    });
}

var server = http.createServer(handleRequest);

server.listen(PORT, function () {
    console.log("Server listening on: http://localhost:%s", PORT);
});