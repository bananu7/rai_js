export default class Worker {
    private _name:string;
    constructor(name:string) {
        this._name=name;
    }
    public Introduce():string { return "Hello I'm " + this._name; }
	public DoJob(task:string) { return "Worker " + this._name + " does " + task; }
} 
