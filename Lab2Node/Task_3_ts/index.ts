import * as http from 'http';
import * as path from 'path';
import * as fs from 'fs';

const PORT = 8080;

function getPathPrefix(ext : string) {
    const exts : { [key:string]:string } = {
        '.jpg': 'images',
        '.png': 'images',
        '.css': 'styles'
    };
    var p = exts[ext];
    return p ? p : "";
}

function handleRequest(req : http.ServerRequest, res : http.ServerResponse) {
    var extension = path.extname(req.url);

    var pathPrefix = getPathPrefix(extension);

    fs.readFile(pathPrefix + req.url, function(err,data){
        if (err) {
            res.statusCode = 404;
            res.end(String(err));
        } else {
            res.end(data);
        }
    });
}

var server = http.createServer(handleRequest);

server.listen(PORT, function () {
    console.log("Server listening on: http://localhost:%s", PORT);
});