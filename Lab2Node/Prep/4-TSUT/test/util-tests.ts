/// <reference path="../node_modules/@types/mocha/index.d.ts" />
/// <reference path="../node_modules/@types/chai/index.d.ts" />
import { expect } from 'chai';
import Hello from '../src/util';

describe('util-tests', function() 
{
	it('should pass this canary test', function() 
	{
		expect(true).to.eql(true);
	});
	
	it('should return proper hello', function() 
	{
		var hello = Hello("all");
		expect(hello).to.eql("Hello to all");
	});
});