/// <reference path="../node_modules/@types/mocha/index.d.ts" />
/// <reference path="../node_modules/@types/chai/index.d.ts" />
import { expect } from 'chai';

declare function require(module:string) : any;
var Worker = require('../src/worker.js');
var Util = require('../src/util.js');

describe('tests', function() 
{
	it('should pass this canary test', function() 
	{
		expect(true).to.eql(true);
	});
	
	var worker : any;
	
	beforeEach(function(){
		worker = new Worker("Axel");
	});	

	it('should return proper invitation', function() 
	{
		var invitation = worker.Introduce();
		expect(invitation).to.eql("Hello I'm Axel");
	});
	
	it('should report work in proper way', function() 
	{
		var report = worker.DoJob("my job");
		expect(report).to.eql("Worker Axel does my job");
	});

	it('should return proper invitation', function() 
	{
		var hello = (new Util()).Hello("all");
		expect(hello).to.eql("Hello to all");
	});
});