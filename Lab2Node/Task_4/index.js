//Lets require/import the HTTP module
var http = require('http');
var path = require('path');
var fs = require('fs');
var url = require('url');
var archiver = require('archiver');

//Lets define a port we want to listen to
const PORT = 8080;

// url:    http://localhost:8080/?folder=images&mask=*.png

//We need a function which handles requests and send response
function handleRequest(request, response) {    
    var url_parts = url.parse(request.url, true);
    var folder = url_parts.query.folder;    
    var mask = url_parts.query.mask;

    // zip output
    var archive = archiver('zip');   
    archive.on('error', function(err){
        console.error(err);
        process.exit(1);
    });

    archive.pipe(response);
    const glob = String(folder) + "/**/" + String(mask)
    console.log(glob);
    archive.glob(glob);
    archive.finalize();
}

//Create a server
var server = http.createServer(handleRequest);

//Lets start our server
server.listen(PORT, function () {
    //Callback triggered when server is successfully listening. Hurray!
    console.log("Server listening on: http://localhost:%s", PORT);
});