var http = require('http');
var path = require('path');
var fs = require('fs');
var url = require('url');
var qs = require('querystring');

const PORT = 8080;

var tasks = {};
function addGrade(id, grade) {
    if (!tasks[id]) {
        tasks[id] = { count: 0, sum: 0 }
    }
    tasks[id].count += 1;
    tasks[id].sum += grade;
}
function getAvg(id) {
    if (!tasks[id] || tasks[id].count == 0) {
        return 0;
    } else {
        return tasks[id].sum / tasks[id].count;
    }
}

function handleRequest(request, response) {
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;

            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);
            
            console.log("Received grade " + post.grade + " for task " + post.taskNumber);

            addGrade(post.taskNumber, parseFloat(post.grade));
            
            response.end("");
        });
    } else {
        var parsedUrl = require('url').parse(request.url, true);

        switch(parsedUrl.pathname) {
            case '/avg':
                var sum = 0;
                var count = 0;
                for (var id in tasks) {
                    sum += getAvg(id);
                    count += 1;
                }
                if (count == 0) {
                    response.end("0");
                } else {
                    response.end(String(sum / count));
                }
                break;
            case '/avgtask':
                response.end(String(getAvg(parsedUrl.query.id)));
                break;

            default:
                fs.readFile('lab1.html', 'utf8', function(err, data) {
                    response.end(data);
                });
                break;
        }
    }
}

var server = http.createServer(handleRequest);

server.listen(PORT, function () {
    console.log("Server listening on: http://localhost:%s", PORT);
});