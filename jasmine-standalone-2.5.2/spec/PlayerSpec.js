	// Zad I.1

describe("Pojazd", function() {
	var p = new Pojazd("p", 10);

	it("properly sets initial speed", function() {
		expect(p.biezacaPredkosc()).toBe(0);
	});
	
	it("properly speeds up", function() {
		p.start(5);
		expect(p.biezacaPredkosc()).toBe(5);
	});
	
	it("properly verifies maximum speed", function() {
		expect(function() {
			p.start(15)
		}).toThrow();
	});
});

// Zad I.2

describe("PojazdProto", function() {
	var p = new PojazdProto("p", 10);

	it("properly sets initial speed", function() {
		expect(p.biezacaPredkosc()).toBe(0);
	});
	
	it("properly verifies maximum speed", function() {
		expect(function() {
			p.start(15)
		}).toThrow();
	});
});

// Zad I.3

describe("PojazdCiezarowy", function() {
	var p = new PojazdCiezarowy("pc", 10, 20);

	it("properly sets initial speed", function() {
		expect(p.biezacaPredkosc()).toBe(0);
	});
	
	it("properly speeds up", function() {
		p.start(5);
		expect(p.biezacaPredkosc()).toBe(5);
	});
	
	it("properly verifies maximum speed", function() {
		expect(function() {
			p.start(15)
		}).toThrow();
	});
	
	it("properly checks for overloading", function() {
		expect(function() {
			p.zaladuj(25)
		}).toThrow();
	});
	
	it("properly checks for over-unloading", function() {
		expect(function() {
			p.rozladuj(10)
		}).toThrow();
	});
});

// Zad I.4

describe("PojazdC", function() {
	var p = new PojazdC("p", 10);

	it("properly sets initial speed", function() {
		expect(p.biezacaPredkosc()).toBe(0);
	});
	
	it("properly verifies maximum speed", function() {
		expect(function() {
			p.start(15)
		}).toThrow();
	});
});

describe("PojazdCiezarowyC", function() {
	var p = new PojazdCiezarowyC("pc", 10, 20);

	it("properly sets initial speed", function() {
		expect(p.biezacaPredkosc()).toBe(0);
	});
	
	it("properly speeds up", function() {
		p.start(5);
		expect(p.biezacaPredkosc()).toBe(5);
	});
	
	it("properly verifies maximum speed", function() {
		expect(function() {
			p.start(15)
		}).toThrow();
	});
	
	it("properly checks for overloading", function() {
		expect(function() {
			p.zaladuj(25)
		}).toThrow();
	});
	
	it("properly checks for over-unloading", function() {
		expect(function() {
			p.rozladuj(10)
		}).toThrow();
	});
});